# modified_pumping_lumen_network

Modified implementation of pumping lumen network based on paper: Dumortier et al. (2019). Hydraulic fracturing and active coarsening position the lumen of the mouse blastocyst. Science, 365(6452), 465 LP – 468. https://doi.org/10.1126/science.aaw7709

**Usage instructions**

The pipeline remains the same as the original implementation. For further detail, please visit

https://github.com/VirtualEmbryo/lumen_network/blob/master/README.md

**Preparation:**  

1. Set the network parameters in the file pattern/[config file].pat 
2. Set the simulation parameters in the file outputs/[simulation].ini 

**Running the simulation:**

3. Go to the outputs folder (cd lumen_network/outputs), then : 
4. Generation of a network ../network/gen_config.py ../pattern/config_embryo.pat 
5. The folder outputs/config will be created with the initial state of the lumen network. 
6. Launch the simulation cd config ; ../../network/simulation.py example.ini 
7. Results of the simulations will be stored in outputs/config/out/ 
8. In addition to the original output files, pumping.dat will also be generated if save_pumping mark is True in the ini file.
