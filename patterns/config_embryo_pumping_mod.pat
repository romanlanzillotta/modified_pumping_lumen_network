[files]
outdir = E:/lumen_network-master/outputs/
tpl = example.ini
subdir = config
nsim = 1

[network]
topology = hexagonal
nlayers = 2
pbc = False
nbicellular = 0
seed = 123

[tensions]
gamma_border = 1.0
gamma_c_border = 1.00

[noisy]
noisy = False
lumen_pos_avg = 0.
lumen_pos_std = 0.1

[volume]
vol_avg = 1
vol_std = 0.1

[chimera]
chimeras = False
gamma_chimera = 0.5

[display]
show = False

[pumping]
modified_pumping = True
lambda_max_avg = 0.0
lambda_max_std = 0
lambda_max_border_avg = 0.007
lambda_max_border_std = 0.1
phi_avg = 0
phi_std = 0
phi_border_avg = 0
phi_border_std = 1.7

